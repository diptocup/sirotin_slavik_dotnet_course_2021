﻿
using System;

namespace Tusk1_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите три числа: ");
            string number = Console.ReadLine();
            int firstDigit = int.Parse(number[0].ToString());
            int secondDigit = int.Parse(number[1].ToString());
            int thirdDigit = int.Parse(number[2].ToString());
            if ((firstDigit > secondDigit) && (firstDigit > thirdDigit))
            {
                Console.WriteLine(firstDigit);
            }
            if ((secondDigit > firstDigit) && (secondDigit > thirdDigit))
            {
                Console.WriteLine(secondDigit);
            }
            if ((thirdDigit > firstDigit) && (thirdDigit > secondDigit))
            {
                Console.WriteLine(thirdDigit);
            }
            Console.ReadKey();
        }
    }
}
