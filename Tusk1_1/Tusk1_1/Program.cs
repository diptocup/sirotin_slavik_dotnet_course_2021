﻿using System;

namespace Tusk1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstNumber = 4;
            int secondNumber = 5;
            Console.WriteLine("firstNumber = {0}, secondNumber = {1}", firstNumber, secondNumber);
            int thirdNumber = firstNumber;
            firstNumber = secondNumber;
            secondNumber = thirdNumber;
            Console.WriteLine("firstNumber = {0}, secondNumber = {1}", firstNumber, secondNumber);
            Console.ReadKey();
        }
    }
}
